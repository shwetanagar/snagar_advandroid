package com.shwetanagar.snagarlab3_1;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class confetti extends Activity {
   // Point pt = new Point();
    private ArrayList<MyPoint> coordinateList = new ArrayList<MyPoint>();
    List<Integer> list = new ArrayList<Integer>();

    int x = 30;
    int y = 30;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new DrawView(this));
    }

    // Inner class to handle drawing to screen.
    class DrawView extends View implements View.OnTouchListener {
        public DrawView(Context context) {
            super(context);
            this.setOnTouchListener(this);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            list.add(Color.RED);
            list.add(Color.GREEN);
            list.add(Color.WHITE);
            list.add(Color.BLUE);
            list.add(Color.YELLOW);
            canvas.drawColor(Color.DKGRAY); // background color
            Paint paint = new Paint();

            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.FILL);
            Rect rect = new Rect();
            String rectInput = "clear";
            canvas.drawRect(30, 30,200,100,paint);
            paint.setTextSize(50);
            paint.setColor(Color.GREEN);
            canvas.drawText(rectInput, 80, 70, paint);
           // canvas.drawText(rectInput, (170-rectInput.length())/2, ((170-rectInput.length())/2)+rectInput.length(), paint) ;
            paint.setColor(Color.RED);

            paint.setTextSize(30);
            canvas.drawText("Tap the screen to add a circle", 250, 50, paint);
           //paint.setColor(Color.WHITE); // color for the circle

            for(MyPoint p: coordinateList){

                paint.setColor(p.getColor());
                //canvas.drawCircle(p.x, p.y, 20, paint);
                int x = p.getPoint().x;
                int y =  p.getPoint().y;
                canvas.drawCircle(x,y,20, paint);

            }

        }

        //@Override
        public boolean onTouch(View view, MotionEvent event) {
            Point pt = new Point();
            MyPoint myPoint = new MyPoint();
            // Every touch generates DOWN ... zero or more MOVEs ... UP.
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                pt.x = (int) event.getX();
                pt.y = (int) event.getY();
                if (pt.x > 30 && pt.x < 200 && pt.y >30 && pt.y < 100){
                    coordinateList.clear();
                } else {
                    Integer color = list.get(new Random().nextInt(list.size()));
                    myPoint.setPoint(pt);
                    myPoint.setColor(color);
                    coordinateList.add(myPoint);
                }
            }
            invalidate();
            return true;
        }
    } // class DrawView



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_confetti, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


