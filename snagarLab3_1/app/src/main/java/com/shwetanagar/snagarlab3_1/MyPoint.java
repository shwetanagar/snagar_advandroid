package com.shwetanagar.snagarlab3_1;

import android.graphics.Color;
import android.graphics.Point;

import java.util.List;

/**
 * Created by shweta on 3/1/15.
 */
public class MyPoint {
    private Point point;
    private Integer color;

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

}
